import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class GraphNode {
    public GraphNode[] neighbors;
    public int value;
    public boolean visited;

    public GraphNode(int v) {
        this.value = v;
        this.visited = false;
    }

    //поиск в ширину
    public void BFS(GraphNode node) {

        Queue<GraphNode> queue = new LinkedList<>();
        node.visited = true;
        queue.add(node);

        System.out.println(node.value);

        while(!queue.isEmpty()) {
            GraphNode v = queue.poll();
            for(GraphNode w : v.neighbors) {
                if(!w.visited) {
                    System.out.println(w.value);
                    w.visited = true;
                    queue.add(w);
                }
            }
        }
    }

    //поиск в глубину
    public void DFS(GraphNode node) {

        Stack<GraphNode> stack = new Stack<>();
        stack.push(node);

        while(!stack.isEmpty()) {
            GraphNode v = stack.pop();
            if(!v.visited) {
                System.out.println(v.value);
                v.visited = true;
                for(int i = v.neighbors.length - 1; i >= 0; i--) {
                    stack.push(v.neighbors[i]);
                }
            }
        }
    }

}
